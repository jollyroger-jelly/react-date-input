import React from 'react';
import {v4 as uuidv4} from 'uuid';

import styles from './WeekdayHeader.module.css';

export default class WeekdayHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let daysOfWeekStrs = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
        let daysOfWeek = new Array(7);
        for (let i = 0; i < daysOfWeek.length; i++) {
            daysOfWeek[i] = <div key={uuidv4()} className={styles.weekdayCell}>{daysOfWeekStrs[i]}</div>
        }

        return (
            <div key={uuidv4()} className={styles.weekdayHeaderBlock}>
                {daysOfWeek}
            </div>
        );
    }
}