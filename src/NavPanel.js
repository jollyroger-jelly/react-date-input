import React from 'react';
import {v4 as uuidv4} from 'uuid';

import styles from './NavPanel.module.css'

export default class NavPanel extends React.Component {
    constructor(props) {
        super(props);
        this.monthName = props.monthName;
        this.year = props.year;
        this.onPrevMonthClick = this.onPrevMonthClick.bind(this);
        this.onNextMonthClick = this.onNextMonthClick.bind(this);
    }

    onPrevMonthClick() {
        this.props.handlePrevMonthClick();
    }

    onNextMonthClick() {
        this.props.handleNextMonthClick();
    }

    render() {
        return (
            <div key={uuidv4()} className={styles.cycleBlock}>
                <input key={uuidv4()} className={styles.cycleLeft} type="button" value="<" onClick={this.onPrevMonthClick}/>
                <div key={uuidv4()} className={styles.yearHeader}>
                    {this.monthName} {this.year}
                </div>
                <input key={uuidv4()} className={styles.cycleRight} type="button" value=">" onClick={this.onNextMonthClick}/>
            </div>
        );
    }
}