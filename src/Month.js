import React from 'react';
import {v4 as uuidv4} from 'uuid';

import { Day } from './Day.js';

export default class Month extends React.Component {
    constructor(props) {
        super(props);
        this.days = props.days;
        this.handleDayClick = this.handleDayClick.bind(this);
    }

    handleDayClick(dayNum) {
        this.props.handleDayClick(dayNum);
    }

    render() {
        let days = new Array(this.days.length)
        for (let i = 0; i < days.length; i++) {
            let innerComps = new Array(this.days[i].length)
            for (let j = 0; j < innerComps.length; j++) {
                let day = this.days[i][j];
                innerComps[j] = <Day key={day.id} id={day.id} number={day.number} isSelected={day.isSelected} handleClick={this.handleDayClick}/>
            }
            days[i] = <div key={uuidv4()}>{innerComps}</div>
        }
        return (
            <div key={uuidv4()}>
                {days}
            </div>
        );
    }
}