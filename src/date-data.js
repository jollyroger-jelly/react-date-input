function getMonthData() {
    let monthData = new Map();
    monthData.set(0, {"name": "January", "days": 31});
    monthData.set(1, {"name": "February", "days": 28});
    monthData.set(2, {"name": "March", "days": 31});
    monthData.set(3, {"name": "April", "days": 30});
    monthData.set(4, {"name": "May", "days": 31});
    monthData.set(5, {"name": "June", "days": 30});
    monthData.set(6, {"name": "July", "days": 31});
    monthData.set(7, {"name": "August", "days": 31});
    monthData.set(8, {"name": "September", "days": 30});
    monthData.set(9, {"name": "October", "days": 31});
    monthData.set(10, {"name": "November", "days": 30});
    monthData.set(11, {"name": "December", "days": 31});
    return monthData;
}

export function getMonthDays(monthIndex, year) {
    let monthData = getMonthData();
    let isLeapYear = year % 4 === 0;
    return monthIndex !== 1 ? (monthData.get(monthIndex)).days : isLeapYear ? 29 : 28;
};

export function getMonthName(monthIndex) {
    let monthData = getMonthData();
    return (monthData.get(monthIndex)).name;
}